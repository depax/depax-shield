# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.4] - 2018-10-24
### Updated

- Amended package script to use `prepublishOnly` instead of `prepare`.

## [1.0.3] - 2018-10-22
### Updated

- Added the prepare hook to compile to prevent publishing the previous release.

## [1.0.2] - 2018-10-22
### Updated

- Amended the Green style to be a brighter green colour.
- Updated README to get the shields from the updated location.

## [1.0.1] - 2018-10-22
### Updated

- Updated README to get the shields from the updated location.
- Updated some of the examples in the README.

## [1.0.0] - 2018-10-01
### Added

- Added CHANGELOG.
- Added LICENSE.
- Added Default/Green, Red and Yellow standard styles.
- Added Flat style.

### Updated

- Renamed the package to "@depax/shield".
- Refactored to use functions and interfaces rather then classes.
- Refactored to use public dev-tool package.

[unlicense]: http://unlicense.org/
[1.0.0]: https://bitbucket.org/depax/depax-shield/src/v1.0.0/
[1.0.1]: https://bitbucket.org/depax/depax-shield/src/v1.0.1/
[1.0.2]: https://bitbucket.org/depax/depax-shield/src/v1.0.2/
[1.0.3]: https://bitbucket.org/depax/depax-shield/src/v1.0.3/
[1.0.4]: https://bitbucket.org/depax/depax-shield/src/v1.0.4/
