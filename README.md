# Depax/Shield

[![CircleCI](https://circleci.com/bb/depax/depax-shield.svg?style=svg)](https://circleci.com/bb/depax/depax-shield)
[![Todos](https://circleci.com/api/v1.1/project/bitbucket/depax/depax-shield/latest/artifacts/0/shields/todos.svg)](#)
[![Features](https://circleci.com/api/v1.1/project/bitbucket/depax/depax-shield/latest/artifacts/0/shields/features.svg)](#)
[![Coverage](https://circleci.com/api/v1.1/project/bitbucket/depax/depax-shield/latest/artifacts/0/shields/features-coverage.svg)](#)
[![Documentation](https://img.shields.io/badge/documentation-🕮-brightgreen.svg?style=plastic)](https://circleci.com/api/v1.1/project/bitbucket/depax/depax-shield/latest/artifacts/0/documentation/index.html)
[![Report](https://img.shields.io/badge/report-💣-brightgreen.svg?style=plastic)](https://circleci.com/api/v1.1/project/bitbucket/depax/depax-shield/latest/artifacts/0/report)

## Installation

Install the package normally using NPM or Yarn.

```sh
yarn add @depax/shield
```

## Usage

To generate a shield with a label and subject, and save it to a file;

```js
import Generate from "@depax/shield";

await Generate("code coverage", "passed", "/filename/of/shield.svg");
```

A shield could also be generated with only the label;

```js
import { GenerateValueless } from "@depax/shield";

await GenerateValueless("documentation", "/filename/of/shield.svg");

// Or you could just send null to the Generate function (this is what GenerateValueless does);
await Generate("code coverage", null, "/filename/of/shield.svg");
```

## Styles

Three styles have been provided internally, these are `GreenShieldStyle`, `RedShieldStyle` and `YellowShieldStyle`. The style can be passed into the `Generate` or `GenerateValueless` functions;

```js
import Generate, { GenerateValueless, YellowShieldStyle } from "@depax/shield";

await Generate("code coverage", "passed", "/filename/of/shield.svg", YellowShieldStyle);

// Or valueless;
await GenerateValueless("code coverage", "/filename/of/shield.svg", YellowShieldStyle);
```

Additionally, custom styles can be created manually;

```js
import Generate, { IShieldStyle } from "@depax/shield";

const MyCustomShieldStyle: IShieldStyle = {
    borderRadius: 4,
    font: { family: "DejaVu Sans,Verdana,Geneva,sans-serif", size: 10 },
    label: { background: "#555", foreground: "#FFF" },
    value: { background: "#97CA00", foreground: "#FFF" },
    xPadding: 10,
    yPadding: 2,
};

await Generate("code coverage", "passed", "/filename/of/shield.svg", MyCustomShieldStyle);
```

## Flat style

By default the shields have a gradient overlay to give a smooth effect. This can be disabled to give a more material or flat feel;

```js
import Generate, { DefaultShieldStyle, GenerateValueless } from "@depax/shield";

await Generate("code coverage", "passed", "/filename/of/shield.svg", DefaultShieldStyle, false);

// Or valueless;
await GenerateValueless("code coverage", "/filename/of/shield.svg", DefaultShieldStyle, false);
```
