@depax-shield-generator @utils
Feature: Provide tests for the "CalcTextSize" util function

  Scenario: The function should return a calculated size of the given text, font and style
     Then The size of "Hello World" using font "Arial" and text size 12 should be 64x14
      And The size of "Hello World" using font "Tahoma" and text size 12 should be 67x15
      And The size of "Hello World" using font "Open Sans" and text size 12 should be 67x15
      And The size of "Hello World" using font "Courier New" and text size 12 should be 77x14
      And The size of "Hello World" using font "Impact" and text size 12 should be 56x16
      And The size of "Hello World" using font "Verdana" and text size 12 should be 67x16
