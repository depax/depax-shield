/**
 * @todo
 */

"use strict";

import chai = require("chai");
import { After, Given, Then } from "cucumber";
import { readFile, unlink } from "fs";
import { tmpdir } from "os";
import { join } from "path";
import { promisify } from "util";
import Generate, {
    DefaultShieldStyle,
    GenerateValueless,
    GreenShieldStyle,
    RedShieldStyle,
    YellowShieldStyle,
} from "../../src";
import CalcTextSize from "../../src/Utils/CalcTextSize";

const readFileP = promisify(readFile);
const unlinkP = promisify(unlink);

let filename: string;

After(async (): Promise<void> => {
    try {
        await unlinkP(filename);
    } catch (err) {
        // Do nothing.
    }
});

Given(/^I generate a shield with the label "(.*)" and value "(.*)"$/, async (
    label: string,
    value: string,
): Promise<void> => {
    filename = join(tmpdir(), "test-shield.svg");
    await Generate(label, value, filename);
});

Given(/^I generate a shield with only the label "(.*)"$/, async (
    label: string,
): Promise<void> => {
    filename = join(tmpdir(), "test-shield.svg");
    await GenerateValueless(label, filename);
});

Given(/^I generate a flat shield with only the label "(.*)"$/, async (
    label: string,
): Promise<void> => {
    filename = join(tmpdir(), "test-shield.svg");
    await GenerateValueless(label, filename, DefaultShieldStyle, true);
});

Given(/^I generate a (green|red|yellow) shield with the label "(.*)" and value "(.*)"$/, async (
    style: string,
    label: string,
    value: string,
): Promise<void> => {
    filename = join(tmpdir(), "test-shield.svg");

    switch (style) {
        case "green":
            await Generate(label, value, filename, GreenShieldStyle);
            break;

        case "red":
            await Generate(label, value, filename, RedShieldStyle);
            break;

        case "yellow":
            await Generate(label, value, filename, YellowShieldStyle);
            break;
    }
});

Given(/^I generate a flat (green|red|yellow) shield with the label "(.*)" and value "(.*)"$/, async (
    style: string,
    label: string,
    value: string,
): Promise<void> => {
    filename = join(tmpdir(), "test-shield.svg");

    switch (style) {
        case "green":
            await Generate(label, value, filename, GreenShieldStyle, true);
            break;

        case "red":
            await Generate(label, value, filename, RedShieldStyle, true);
            break;

        case "yellow":
            await Generate(label, value, filename, YellowShieldStyle, true);
            break;
    }
});

Then(/^The size of "(.*)" using font "(.*)" and text size ([0-9]*) should be ([0-9]*)x([0-9]*)$/, async (
    text: string,
    font: string,
    size: number,
    width: number,
    height: number,
): Promise<void> => {
    const [ cWidth, cHeight ] = CalcTextSize(text, font, size);
    chai.assert.equal(cWidth, width);
    chai.assert.equal(cHeight, height);
});

Then(/^The generated shield SVG should be:$/, async (
    payload: string,
): Promise<void> => {
    const svg = (await readFileP(filename)).toString();
    chai.assert.equal(svg, payload);
});
