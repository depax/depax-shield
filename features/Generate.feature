@depax-shield-generator @generate
Feature: Provides the test for the shield generator function

  Scenario: Should generate a shield with the default style
    Given I generate a shield with the label "Hello" and value "World"
     Then The generated shield SVG should be:
      """
      <svg xmlns="http://www.w3.org/2000/svg" width="94" height="17">
      <linearGradient id="overlay" x2="0" y2="100%">
          <stop offset="0" stop-color="#FFF" stop-opacity=".7"/>
          <stop offset=".1" stop-color="#AAA" stop-opacity=".1"/>
          <stop offset=".9" stop-color="#000" stop-opacity=".3"/>
          <stop offset="1" stop-color="#000" stop-opacity=".5"/>
      </linearGradient>
      <mask id="shield">
          <rect width="100%" height="100%" rx="4" fill="#FFF"/>
      </mask>
      <g mask="url(#shield)">
          <rect width="46" height="100%" fill="#555"/>
          <rect x="46" width="48" height="100%" fill="#4C1"/>
          <rect width="100%" height="100%" fill="url(#overlay)"/>
      </g>
      <svg width="46" height="100%">
          <g fill="#FFF" font-family="DejaVu Sans,Verdana,Geneva,sans-serif" font-size="10">
              <text x="50%" y="50%" dy="1" text-anchor="middle" alignment-baseline="middle" fill="#010101" fill-opacity=".3">Hello</text>
              <text x="50%" y="50%" text-anchor="middle" alignment-baseline="middle">Hello</text>
          </g>
      </svg>
      <svg x="46" width="48" height="100%">
          <g fill="#FFF" font-family="DejaVu Sans,Verdana,Geneva,sans-serif" font-size="10">
              <text x="50%" y="50%" dy="1" text-anchor="middle" alignment-baseline="middle" fill="#010101" fill-opacity=".3">World</text>
              <text x="50%" y="50%" text-anchor="middle" alignment-baseline="middle">World</text>
          </g>
      </svg>
      </svg>
      """

  Scenario: Should generate a shield with the green style
    Given I generate a green shield with the label "Hello" and value "World"
     Then The generated shield SVG should be:
      """
      <svg xmlns="http://www.w3.org/2000/svg" width="94" height="17">
      <linearGradient id="overlay" x2="0" y2="100%">
          <stop offset="0" stop-color="#FFF" stop-opacity=".7"/>
          <stop offset=".1" stop-color="#AAA" stop-opacity=".1"/>
          <stop offset=".9" stop-color="#000" stop-opacity=".3"/>
          <stop offset="1" stop-color="#000" stop-opacity=".5"/>
      </linearGradient>
      <mask id="shield">
          <rect width="100%" height="100%" rx="4" fill="#FFF"/>
      </mask>
      <g mask="url(#shield)">
          <rect width="46" height="100%" fill="#555"/>
          <rect x="46" width="48" height="100%" fill="#4C1"/>
          <rect width="100%" height="100%" fill="url(#overlay)"/>
      </g>
      <svg width="46" height="100%">
          <g fill="#FFF" font-family="DejaVu Sans,Verdana,Geneva,sans-serif" font-size="10">
              <text x="50%" y="50%" dy="1" text-anchor="middle" alignment-baseline="middle" fill="#010101" fill-opacity=".3">Hello</text>
              <text x="50%" y="50%" text-anchor="middle" alignment-baseline="middle">Hello</text>
          </g>
      </svg>
      <svg x="46" width="48" height="100%">
          <g fill="#FFF" font-family="DejaVu Sans,Verdana,Geneva,sans-serif" font-size="10">
              <text x="50%" y="50%" dy="1" text-anchor="middle" alignment-baseline="middle" fill="#010101" fill-opacity=".3">World</text>
              <text x="50%" y="50%" text-anchor="middle" alignment-baseline="middle">World</text>
          </g>
      </svg>
      </svg>
      """

  Scenario: Should generate a shield with the red style
    Given I generate a red shield with the label "Hello" and value "World"
     Then The generated shield SVG should be:
      """
      <svg xmlns="http://www.w3.org/2000/svg" width="94" height="17">
      <linearGradient id="overlay" x2="0" y2="100%">
          <stop offset="0" stop-color="#FFF" stop-opacity=".7"/>
          <stop offset=".1" stop-color="#AAA" stop-opacity=".1"/>
          <stop offset=".9" stop-color="#000" stop-opacity=".3"/>
          <stop offset="1" stop-color="#000" stop-opacity=".5"/>
      </linearGradient>
      <mask id="shield">
          <rect width="100%" height="100%" rx="4" fill="#FFF"/>
      </mask>
      <g mask="url(#shield)">
          <rect width="46" height="100%" fill="#555"/>
          <rect x="46" width="48" height="100%" fill="#E05D44"/>
          <rect width="100%" height="100%" fill="url(#overlay)"/>
      </g>
      <svg width="46" height="100%">
          <g fill="#FFF" font-family="DejaVu Sans,Verdana,Geneva,sans-serif" font-size="10">
              <text x="50%" y="50%" dy="1" text-anchor="middle" alignment-baseline="middle" fill="#010101" fill-opacity=".3">Hello</text>
              <text x="50%" y="50%" text-anchor="middle" alignment-baseline="middle">Hello</text>
          </g>
      </svg>
      <svg x="46" width="48" height="100%">
          <g fill="#FFF" font-family="DejaVu Sans,Verdana,Geneva,sans-serif" font-size="10">
              <text x="50%" y="50%" dy="1" text-anchor="middle" alignment-baseline="middle" fill="#010101" fill-opacity=".3">World</text>
              <text x="50%" y="50%" text-anchor="middle" alignment-baseline="middle">World</text>
          </g>
      </svg>
      </svg>
      """

  Scenario: Should generate a shield with the yellow style
    Given I generate a yellow shield with the label "Hello" and value "World"
     Then The generated shield SVG should be:
      """
      <svg xmlns="http://www.w3.org/2000/svg" width="94" height="17">
      <linearGradient id="overlay" x2="0" y2="100%">
          <stop offset="0" stop-color="#FFF" stop-opacity=".7"/>
          <stop offset=".1" stop-color="#AAA" stop-opacity=".1"/>
          <stop offset=".9" stop-color="#000" stop-opacity=".3"/>
          <stop offset="1" stop-color="#000" stop-opacity=".5"/>
      </linearGradient>
      <mask id="shield">
          <rect width="100%" height="100%" rx="4" fill="#FFF"/>
      </mask>
      <g mask="url(#shield)">
          <rect width="46" height="100%" fill="#555"/>
          <rect x="46" width="48" height="100%" fill="#C5C520"/>
          <rect width="100%" height="100%" fill="url(#overlay)"/>
      </g>
      <svg width="46" height="100%">
          <g fill="#FFF" font-family="DejaVu Sans,Verdana,Geneva,sans-serif" font-size="10">
              <text x="50%" y="50%" dy="1" text-anchor="middle" alignment-baseline="middle" fill="#010101" fill-opacity=".3">Hello</text>
              <text x="50%" y="50%" text-anchor="middle" alignment-baseline="middle">Hello</text>
          </g>
      </svg>
      <svg x="46" width="48" height="100%">
          <g fill="#FFF" font-family="DejaVu Sans,Verdana,Geneva,sans-serif" font-size="10">
              <text x="50%" y="50%" dy="1" text-anchor="middle" alignment-baseline="middle" fill="#010101" fill-opacity=".3">World</text>
              <text x="50%" y="50%" text-anchor="middle" alignment-baseline="middle">World</text>
          </g>
      </svg>
      </svg>
      """

  Scenario: Should generate a shield with the flat green style
    Given I generate a flat green shield with the label "Hello" and value "World"
     Then The generated shield SVG should be:
      """
      <svg xmlns="http://www.w3.org/2000/svg" width="94" height="17">
      <mask id="shield">
          <rect width="100%" height="100%" rx="4" fill="#FFF"/>
      </mask>
      <g mask="url(#shield)">
          <rect width="46" height="100%" fill="#555"/>
          <rect x="46" width="48" height="100%" fill="#4C1"/>
      </g>
      <svg width="46" height="100%">
          <g fill="#FFF" font-family="DejaVu Sans,Verdana,Geneva,sans-serif" font-size="10">
              <text x="50%" y="50%" dy="1" text-anchor="middle" alignment-baseline="middle" fill="#010101" fill-opacity=".3">Hello</text>
              <text x="50%" y="50%" text-anchor="middle" alignment-baseline="middle">Hello</text>
          </g>
      </svg>
      <svg x="46" width="48" height="100%">
          <g fill="#FFF" font-family="DejaVu Sans,Verdana,Geneva,sans-serif" font-size="10">
              <text x="50%" y="50%" dy="1" text-anchor="middle" alignment-baseline="middle" fill="#010101" fill-opacity=".3">World</text>
              <text x="50%" y="50%" text-anchor="middle" alignment-baseline="middle">World</text>
          </g>
      </svg>
      </svg>
      """

  Scenario: Should generate a shield with the flat red style
    Given I generate a flat red shield with the label "Hello" and value "World"
     Then The generated shield SVG should be:
      """
      <svg xmlns="http://www.w3.org/2000/svg" width="94" height="17">
      <mask id="shield">
          <rect width="100%" height="100%" rx="4" fill="#FFF"/>
      </mask>
      <g mask="url(#shield)">
          <rect width="46" height="100%" fill="#555"/>
          <rect x="46" width="48" height="100%" fill="#E05D44"/>
      </g>
      <svg width="46" height="100%">
          <g fill="#FFF" font-family="DejaVu Sans,Verdana,Geneva,sans-serif" font-size="10">
              <text x="50%" y="50%" dy="1" text-anchor="middle" alignment-baseline="middle" fill="#010101" fill-opacity=".3">Hello</text>
              <text x="50%" y="50%" text-anchor="middle" alignment-baseline="middle">Hello</text>
          </g>
      </svg>
      <svg x="46" width="48" height="100%">
          <g fill="#FFF" font-family="DejaVu Sans,Verdana,Geneva,sans-serif" font-size="10">
              <text x="50%" y="50%" dy="1" text-anchor="middle" alignment-baseline="middle" fill="#010101" fill-opacity=".3">World</text>
              <text x="50%" y="50%" text-anchor="middle" alignment-baseline="middle">World</text>
          </g>
      </svg>
      </svg>
      """

  Scenario: Should generate a shield with the flat yellow style
    Given I generate a flat yellow shield with the label "Hello" and value "World"
     Then The generated shield SVG should be:
      """
      <svg xmlns="http://www.w3.org/2000/svg" width="94" height="17">
      <mask id="shield">
          <rect width="100%" height="100%" rx="4" fill="#FFF"/>
      </mask>
      <g mask="url(#shield)">
          <rect width="46" height="100%" fill="#555"/>
          <rect x="46" width="48" height="100%" fill="#C5C520"/>
      </g>
      <svg width="46" height="100%">
          <g fill="#FFF" font-family="DejaVu Sans,Verdana,Geneva,sans-serif" font-size="10">
              <text x="50%" y="50%" dy="1" text-anchor="middle" alignment-baseline="middle" fill="#010101" fill-opacity=".3">Hello</text>
              <text x="50%" y="50%" text-anchor="middle" alignment-baseline="middle">Hello</text>
          </g>
      </svg>
      <svg x="46" width="48" height="100%">
          <g fill="#FFF" font-family="DejaVu Sans,Verdana,Geneva,sans-serif" font-size="10">
              <text x="50%" y="50%" dy="1" text-anchor="middle" alignment-baseline="middle" fill="#010101" fill-opacity=".3">World</text>
              <text x="50%" y="50%" text-anchor="middle" alignment-baseline="middle">World</text>
          </g>
      </svg>
      </svg>
      """

  Scenario: Should generate a shield with the default style and no value panel
    Given I generate a shield with only the label "Hello World"
     Then The generated shield SVG should be:
      """
      <svg xmlns="http://www.w3.org/2000/svg" width="77" height="17">
      <linearGradient id="overlay" x2="0" y2="100%">
          <stop offset="0" stop-color="#FFF" stop-opacity=".7"/>
          <stop offset=".1" stop-color="#AAA" stop-opacity=".1"/>
          <stop offset=".9" stop-color="#000" stop-opacity=".3"/>
          <stop offset="1" stop-color="#000" stop-opacity=".5"/>
      </linearGradient>
      <mask id="shield">
          <rect width="100%" height="100%" rx="4" fill="#FFF"/>
      </mask>
      <g mask="url(#shield)">
          <rect width="100%" height="100%" fill="#555"/>
          <rect width="100%" height="100%" fill="url(#overlay)"/>
      </g>
      <svg width="77" height="100%">
          <g fill="#FFF" font-family="DejaVu Sans,Verdana,Geneva,sans-serif" font-size="10">
              <text x="50%" y="50%" dy="1" text-anchor="middle" alignment-baseline="middle" fill="#010101" fill-opacity=".3">Hello World</text>
              <text x="50%" y="50%" text-anchor="middle" alignment-baseline="middle">Hello World</text>
          </g>
      </svg>
      </svg>
      """

  Scenario: Should generate a shield with the default style, flat and no value panel
    Given I generate a flat shield with only the label "Hello World"
     Then The generated shield SVG should be:
      """
      <svg xmlns="http://www.w3.org/2000/svg" width="77" height="17">
      <mask id="shield">
          <rect width="100%" height="100%" rx="4" fill="#FFF"/>
      </mask>
      <g mask="url(#shield)">
          <rect width="100%" height="100%" fill="#555"/>
      </g>
      <svg width="77" height="100%">
          <g fill="#FFF" font-family="DejaVu Sans,Verdana,Geneva,sans-serif" font-size="10">
              <text x="50%" y="50%" dy="1" text-anchor="middle" alignment-baseline="middle" fill="#010101" fill-opacity=".3">Hello World</text>
              <text x="50%" y="50%" text-anchor="middle" alignment-baseline="middle">Hello World</text>
          </g>
      </svg>
      </svg>
      """
