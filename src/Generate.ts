/**
 * Provides the main file responsible for generating the shields and saving them.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import { writeFile } from "fs";
import { promisify } from "util";
import IShieldStyle, { DefaultShieldStyle } from "./Interfaces/IShieldStyle";
import CalcTextSize from "./Utils/CalcTextSize";
import StrReplaceAll from "./Utils/StrReplaceAll";

const writeFileP = promisify(writeFile);

/** The SVG template for the gradient. */
const ShieldGradient: string = `<linearGradient id="overlay" x2="0" y2="100%">
    <stop offset="0" stop-color="#FFF" stop-opacity=".7"/>
    <stop offset=".1" stop-color="#AAA" stop-opacity=".1"/>
    <stop offset=".9" stop-color="#000" stop-opacity=".3"/>
    <stop offset="1" stop-color="#000" stop-opacity=".5"/>
</linearGradient>`;

/** The SVG template for the shield mask. */
const ShieldMask: string = `<mask id="shield">
    <rect width="100%" height="100%" rx="{{borderRadius}}" fill="#FFF"/>
</mask>`;

/** The SVG template for the shield body when we have a label and a value. */
const ShieldBodyWithValue: string = `<g mask="url(#shield)">
    <rect width="{{labelWidth}}" height="100%" fill="{{labelBackgroundColour}}"/>
    <rect x="{{labelWidth}}" width="{{valueWidth}}" height="100%" fill="{{valueBackgroundColour}}"/>
    <rect width="100%" height="100%" fill="url(#overlay)"/>
</g>`;

/** The SVG template for the shield body when we have a label and a value, but for a flat look. */
const ShieldFlatBodyWithValue: string = `<g mask="url(#shield)">
    <rect width="{{labelWidth}}" height="100%" fill="{{labelBackgroundColour}}"/>
    <rect x="{{labelWidth}}" width="{{valueWidth}}" height="100%" fill="{{valueBackgroundColour}}"/>
</g>`;

/** The SVG template for the shield body when we only have a label. */
const ShieldBodyWithoutValue: string = `<g mask="url(#shield)">
    <rect width="100%" height="100%" fill="{{labelBackgroundColour}}"/>
    <rect width="100%" height="100%" fill="url(#overlay)"/>
</g>`;

/** The SVG template for the shield body when we only have a label, but for a flat look. */
const ShieldFlatBodyWithoutValue: string = `<g mask="url(#shield)">
    <rect width="100%" height="100%" fill="{{labelBackgroundColour}}"/>
</g>`;

/** The SVG template for the label panel. */
const ShieldLabelPanel: string = `<svg width="{{labelWidth}}" height="100%">
    <g fill="{{labelFontColour}}" font-family="{{fontFamily}}" font-size="{{fontSize}}">
        <text x="50%" y="50%" dy="1" text-anchor="middle" alignment-baseline="middle" fill="#010101" ` +
            `fill-opacity=".3">{{label}}</text>
        <text x="50%" y="50%" text-anchor="middle" alignment-baseline="middle">{{label}}</text>
    </g>
</svg>`;

/** The SVG template for the value panel. */
const ShieldValuePanel: string = `<svg x="{{labelWidth}}" width="{{valueWidth}}" height="100%">
    <g fill="{{valueFontColour}}" font-family="{{fontFamily}}" font-size="{{fontSize}}">
        <text x="50%" y="50%" dy="1" text-anchor="middle" alignment-baseline="middle" fill="#010101" ` +
            `fill-opacity=".3">{{value}}</text>
        <text x="50%" y="50%" text-anchor="middle" alignment-baseline="middle">{{value}}</text>
    </g>
</svg>`;

/**
 * Generate the shield SVG and save it to the provided filename.
 *
 * @param label The label to apply to shield.
 * @param value The value to apply to the shield.
 * @param filename The filename to save the generate shield SVG.
 * @param style The style to apply to the shield, defaults to <b>DefaultShieldStyle</b>.
 * @param flat If the shield should be flat or have a plastic like look.
 */
export default async function Generate(
    label: string,
    value: string,
    filename: string,
    style: IShieldStyle = DefaultShieldStyle,
    flat: boolean = false,
): Promise<void> {
    // Calculate the width and height of the label text.
    let [ labelWidth, labelHeight ] = CalcTextSize(label, style.font.family, style.font.size);
    labelWidth += style.xPadding * 2;
    labelHeight += style.yPadding * 2;

    // Calculate the width and height of the value text.
    let [ valueWidth, valueHeight ] = CalcTextSize(value || "", style.font.family, style.font.size);
    valueWidth += style.xPadding * 2;
    valueHeight += style.yPadding * 2;

    // Calculate the overall width and hight of the shield.
    const width = value ? labelWidth + valueWidth : labelWidth;
    const height = value ? Math.max(labelHeight, valueHeight) : labelHeight;

    // Start building the SVG output.
    const data = [];
    data.push(`<svg xmlns="http://www.w3.org/2000/svg" width="${width}" height="${height}">`);

    // If this is a flat shield, then dont include the gradient template.
    if (!flat) {
        data.push(ShieldGradient);
    }

    // Add the mask template.
    data.push(ShieldMask);

    // Add the shield body template, depending on the style and if value is provided.
    if (!value) {
        data.push(flat ? ShieldFlatBodyWithoutValue : ShieldBodyWithoutValue);
    } else {
        data.push(flat ? ShieldFlatBodyWithValue : ShieldBodyWithValue);
    }

    // Add the label panel template.
    data.push(ShieldLabelPanel);

    // Add the value panel template, if applicable.
    if (value) {
        data.push(ShieldValuePanel);
    }

    data.push("</svg>");

    // Perform a token replacement on the generated SVG data, and save the file.
    await writeFileP(filename, StrReplaceAll(data.join("\n"), {
        borderRadius: style.borderRadius,
        fontFamily: style.font.family,
        fontSize: style.font.size,
        height,
        label,
        labelBackgroundColour: style.label.background,
        labelFontColour: style.label.foreground,
        labelWidth,
        value,
        valueBackgroundColour: style.value.background,
        valueFontColour: style.label.foreground,
        valueWidth,
        width,
    }));
}

/**
 * Exeactly the same as the <b>Generate</b> function, but without a value.
 *
 * @param label The label to apply to shield.
 * @param filename The filename to save the generate shield SVG.
 * @param style The style to apply to the shield, defaults to <b>DefaultShieldStyle</b>.
 * @param flat If the shield should be flat or have a plastic like look.
 */
export async function GenerateValueless(
    label: string,
    filename: string,
    style: IShieldStyle = DefaultShieldStyle,
    flat: boolean = false,
): Promise<void> {
    return await Generate(label, null, filename, style, flat);
}
