/**
 * Provides the interfaces to desribe the various aspects of the shield style definition.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

/** An interface to describe the font style. */
export interface IShieldFont {
    /** The font family. */
    family: string;

    /** The size of the font. */
    size: number;
}

/** An interface to describe the shield panel type. */
export interface IShieldPanel {
    /** The panel's background colour. */
    background: string;

    /** The panel's text colour. */
    foreground: string;
}

/** An interface to describe the shield style. */
export default interface IShieldStyle {
    /** The radius of the rounded border. */
    borderRadius: number;

    /** The padding to apply to the width, note this gets doubled for either side of the text. */
    xPadding: number;

    /** The padding to apply to the height, note this gets doubled for either side of the text. */
    yPadding: number;

    /** The font settings. */
    font: IShieldFont;

    /** The label panel settings. */
    label: IShieldPanel;

    /** The value panel settings. */
    value: IShieldPanel;
}

/** The default shield style. */
export const DefaultShieldStyle: IShieldStyle = {
    borderRadius: 4,
    font: { family: "DejaVu Sans,Verdana,Geneva,sans-serif", size: 10 },
    label: { background: "#555", foreground: "#FFF" },
    value: { background: "#4C1", foreground: "#FFF" },
    xPadding: 10,
    yPadding: 2,
};

/** The green or success shield style. */
export const GreenShieldStyle = DefaultShieldStyle;

/** The red or failed shield style. */
export const RedShieldStyle: IShieldStyle = {
    borderRadius: 4,
    font: { family: "DejaVu Sans,Verdana,Geneva,sans-serif", size: 10 },
    label: { background: "#555", foreground: "#FFF" },
    value: { background: "#E05D44", foreground: "#FFF" },
    xPadding: 10,
    yPadding: 2,
};

/** The yellow or warning shield style. */
export const YellowShieldStyle: IShieldStyle = {
    borderRadius: 4,
    font: { family: "DejaVu Sans,Verdana,Geneva,sans-serif", size: 10 },
    label: { background: "#555", foreground: "#FFF" },
    value: { background: "#C5C520", foreground: "#FFF" },
    xPadding: 10,
    yPadding: 2,
};
