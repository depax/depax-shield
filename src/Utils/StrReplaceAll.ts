/**
 * Provides the StrReplaceAll utility function.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

/**
 * A simple interface to explain the tokens structure.
 */
export interface ITokens {
    /** The token and value key/value pair. */
    [token: string]: string|number;
}

/**
 * Perform a token replacement on the given string.
 *
 * @param value The string value to perform the token replacement on.
 * @param tokens A collection of tokens and values.
 *
 * @return Returns the string with the matching tokens replaced.
 */
export default function StrReplaceAll(value: string, tokens: ITokens): string {
    let replaced = value;
    Object.keys(tokens).forEach((token) => {
        replaced = replaced.replace(new RegExp(`{{${token}}}`, "g"), `${tokens[token]}`);
    });

    return replaced;
}
