/**
 * Provides the CalcTextSize utility function.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import { createCanvas } from "canvas-prebuilt";

/**
 * Attempts to calculate the size of a given text.
 *
 * @param text The text to calculate.
 * @param font The font family to use.
 * @param size The size of the font to use.
 *
 * @return Returns a 2 element array with the width and height.
 */
export default function CalcTextSize(
    text: string,
    font: string,
    size: number,
): number[] {
    const canvas = createCanvas(200, 200);
    const ctx = canvas.getContext("2d");
    ctx.font = `${size}px ${font}`;

    const measurement = ctx.measureText(text);
    return [
        measurement.width,
        measurement.emHeightAscent + measurement.emHeightDescent,
    ];
}
