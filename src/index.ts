/**
 * Provides the route file of the module, exposing all the relevant items.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import Generate, { GenerateValueless } from "./Generate";
import IShieldStyle, {
    DefaultShieldStyle,
    GreenShieldStyle,
    IShieldFont, IShieldPanel,
    RedShieldStyle,
    YellowShieldStyle,
} from "./Interfaces/IShieldStyle";

export default Generate;
export {
    GenerateValueless,

    IShieldFont, IShieldPanel, IShieldStyle,

    DefaultShieldStyle,
    GreenShieldStyle,
    RedShieldStyle,
    YellowShieldStyle,
};
